sap.ui.define(["de/fpf/test/ui5testapp/controller/BaseController"], function (Controller) {
    "use strict";

    return Controller.extend("de.fpf.test.ui5testapp.controller.MainView", {});
});
