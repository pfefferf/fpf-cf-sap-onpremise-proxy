const axios = require("axios");

module.exports = async (destinationName, destUri, token) => {
  const destSrvUrl =
    destUri + "/destination-configuration/v1/destinations/" + destinationName;
  const config = {
    headers: {
      Authorization: "Bearer " + token,
    },
  };
  const res = await axios.get(destSrvUrl, config);
  return res.data;
};
