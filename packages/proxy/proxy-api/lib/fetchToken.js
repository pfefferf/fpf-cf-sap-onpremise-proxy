const axios = require("axios");

const tokenCache = [];

module.exports = async (oauthUrl, oauthClient, oauthSecret) => {
  const tokenIdx = tokenCache.findIndex((cacheEntry) => {
    return cacheEntry.oauthUrl === oauthUrl && cacheEntry.oauthClient === oauthClient;
  })

  if(tokenIdx !== -1) {
    const cachedToken = tokenCache[tokenIdx];
    if(Math.abs((new Date().getTime() - cachedToken.created.getTime()) / 1000) > 180) {
      tokenCache.splice(tokenIdx, 1);
    } else {
      return cachedToken.token;
    }
  }

  const tokenUrl = oauthUrl + "/oauth/token?grant_type=client_credentials&response_type=token";
  const config = {
     headers: {
        Authorization: "Basic " + Buffer.from(oauthClient + ':' + oauthSecret).toString("base64")
     }
  };
  const res = await axios.get(tokenUrl, config);
  const access_token = res.data.access_token;
  
  tokenCache.push({
    token: access_token,
    oauthUrl: oauthUrl,
    oauthClient: oauthClient,
    created: new Date()
  });
  
  return access_token;
}  