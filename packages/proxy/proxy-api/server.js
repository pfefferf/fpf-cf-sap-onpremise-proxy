const express = require("express");
const passport = require("passport");
const xssec = require("@sap/xssec"); 
const xsenv = require("@sap/xsenv");
const cfenv = require("cfenv");
const axios = require("axios");

const fetchToken = require("./lib/fetchToken");
const readDestination = require("./lib/readDestination");

const app = express();
const port = process.env.PORT || 3001;

passport.use("JWT", new xssec.JWTStrategy(xsenv.getServices({
	uaa: {
		tag: "xsuaa"
	}
}).uaa));

app.use(passport.initialize());
app.use(passport.authenticate("JWT", { session: false }));

app.use(express.raw({ limit: "20MB", type: "*/*" }));

app.use("/info", (req, res) => {
  res.json({
    "description": "fpf-cf-sap-onpremise-proxy",
    "version": "1.0.0"
  });
});

app.use("/deploy/*", async (req, res) => {
  try{
    const urlParts = req.originalUrl.split("/");
    urlParts.shift(); // remove empty element
    urlParts.shift(); // remove deploy
    const destinationName = urlParts.shift();
    const targetUrl = "/" + urlParts.join("/");

    const destCredentials = cfenv.getAppEnv().getService("fpf-cf-sap-onpremise-proxy-dest").credentials;
    const connCredentials = cfenv.getAppEnv().getService("fpf-cf-sap-onpremise-proxy-conn").credentials;    
    const destToken = await fetchToken(destCredentials.url, destCredentials.clientid, destCredentials.clientsecret);
    const destination = await readDestination(destinationName, destCredentials.uri, destToken);
    const connToken = await fetchToken(connCredentials.url, connCredentials.clientid, connCredentials.clientsecret);    
    
    const reqOptions = {
      method: req.method,
      url: destination.destinationConfiguration.URL + targetUrl,
      headers: {    
        "Proxy-Authorization": "Bearer " + connToken
      },
      proxy: {
        host: connCredentials.onpremise_proxy_host,
        port: connCredentials.onpremise_proxy_http_port
      },
      maxBodyLength: Infinity,
      maxContentLength: Infinity
    };

    reqOptions.headers["authorization"] = `${destination.authTokens[0].type} ${destination.authTokens[0].value}`; // pre-condition: destination with credentials

    ["accept", "x-csrf-token", "content-type", "cookie", "accept-language", "if-match"].forEach((headerIdentifier) => {
      if(req.headers[headerIdentifier]) {
        reqOptions.headers[headerIdentifier] = req.headers[headerIdentifier];
      }
    });

    if(req.method !== "GET" && req.method !== "DELETE" && req.body) {
      reqOptions.data = Buffer.from(req.body).toString("utf-8");
    }

    reqOptions.validateStatus = (status) => {
      return status < 999;
    };

    const resOnPrem = await axios(reqOptions); 
    res.status(resOnPrem.status).set(resOnPrem.headers).send(resOnPrem.data);
  } catch (error) {
    console.log(error);
    res.status(500).send(error);
  }
});

app.listen(port, () => {
  console.log(`Proxy listening on port ${port}`);
});
