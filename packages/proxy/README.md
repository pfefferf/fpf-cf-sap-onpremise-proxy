# On-Premise Proxy running on SAP BTP CF to access SAP ABAP On-Premise systems

## Why is it needed?

Deployment from Bitbucket Cloud Pipelines to On-Premise systems via Proxy on SAP BTP.
This is relevant as long a Bitbucket Pipeline Runners for On-Premise are not available.

## Usages

### Deployment

![image](./doc/architecture_diagram.svg "Architecture")

The `/deploy` endpoint provides the option to deploy UI5/Vue applications to an On-Premise SAP system. The On-Premise SAP system must be connected to SAP BTP
via SAP Cloud Connector. The destination must provide the credentials for authorization against the backend system.
  
Which is destination is used, is added to the to the path of the deploy URL. E.g. for `/deploy/devah4` the destination `deva4h` is used.
